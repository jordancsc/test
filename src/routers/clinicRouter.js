
(function () {

    ClinicRouter = Backbone.Router.extend({
        routes: {
            "clinic-details/:id": "viewClinicById",
            "": "index"
        },
        initialize: function () {
            this.clinicsCollection = new Clinics();
            this.clinicsCollection.fetch();
        },
        viewClinicById: function (id) {
            var model = this.clinicsCollection.get(id);

            var view = new ClinicDetailsView(
                {
                    el: "#container",
                    model: model
                });

            view.render();
        },
        index: function () {
            var view = new ClinicsView({
                el: "#container",
                model: this.clinicsCollection
            });
            $("body").append(view.render().$el);
        }
    });
}());